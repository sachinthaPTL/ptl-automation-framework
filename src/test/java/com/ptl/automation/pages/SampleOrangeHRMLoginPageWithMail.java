package com.ptl.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SampleOrangeHRMLoginPageWithMail extends BasePage {
    public SampleOrangeHRMLoginPageWithMail(WebDriver driver) {
        super(driver);
    }

    //Elements
    @FindBy(id = "txtUsername")
    WebElement userName;

    @FindBy(id = "txtPassword")
    WebElement password;

    @FindBy(name = "Submit")
    WebElement submit;

    @FindBy(name = "welcome")
    WebElement welcome;

    public void typeUserName(){
        userName.sendKeys("Admin");
    }

    public void typePassword(){
        password.sendKeys("admin");
    }

    public void clickSubmit(){
        submit.click();
    }

    public void assertion(WebElement welcome){

    }


}
