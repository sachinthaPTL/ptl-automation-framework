package com.ptl.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class SamplePage extends BasePage{

    // Page elements
    @FindBy(name = "firstname")
    private WebElement txtName;

    public SamplePage(WebDriver driver) {
        super(driver);
    }

    public void fillDetails(){
        txtName.sendKeys("Test");
    }
}
