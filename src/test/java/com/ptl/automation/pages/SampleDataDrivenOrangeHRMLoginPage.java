package com.ptl.automation.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SampleDataDrivenOrangeHRMLoginPage extends BasePage {
    public SampleDataDrivenOrangeHRMLoginPage(WebDriver driver) {
        super(driver);
    }

    //Elements
    @FindBy(id = "txtUsername")
    WebElement userName;

    @FindBy(id = "txtPassword")
    WebElement password;

    @FindBy(name = "Submit")
    WebElement submit;

    public void typeUserName(String UserName){
        userName.sendKeys(UserName);
    }

    public void typePassword(String Password){
        password.sendKeys(Password);
    }

    public void clickSubmit(){
        submit.click();
    }

}
