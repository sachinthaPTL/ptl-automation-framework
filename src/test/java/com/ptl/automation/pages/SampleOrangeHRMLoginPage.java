package com.ptl.automation.pages;

import com.ptl.automation.test.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SampleOrangeHRMLoginPage extends TestBase {
//    public SampleOrangeHRMLoginPage(WebDriver driver) {
//        super(driver);
//    }

    //Elements
    @FindBy(id = "txtUsername")
    private WebElement userName;

    @FindBy(id = "txtPassword")
    private WebElement password;

    @FindBy(name = "Submit")
    private WebElement submit;

    @FindBy(id = "welcome")
    private WebElement welcome;

    public void typeUserName() {
        sendKeys(userName,"Admin");
    }

    public void typePassword(){
        sendKeys(password,"admin");
    }

    public void clickSubmit(){
        submit(submit);
    }

    public boolean assertionPass(){
        welcome.isDisplayed();
        return true;
    }

}
