package com.ptl.automation.util.property;


public enum PropertyEnum {
    BASE_URL("BASE_URL", ""),

    ADMIN_USER_NAME("ADMIN_USER_NAME", ""),
    ADMIN_USER_PASSWORD("ADMIN_USER_PASSWORD", ""),

    BROWSER_IE("BROWSER_IE",""),
    BROWSER_CHROME("BROWSER_CHROME",""),
    BROWSER_FIREFOX("BROWSER_FIREFOX",""),
    BROWSER_HTMLUNIT("BROWSER_HTMLUNIT",""),
    BROWSER_PHANTHOMJS("BROWSER_PHANTHOMJS",""),


    // Extent Reports Parameters
    EXTENDREPORT_PATH("EXTENDREPORT_PATH",""),
    SCREENSHOT_PATH("SCREENSHOT_PATH",""),


    //Email Parameters
    USERNAME("USERNAME",""),
    PASSWORD("PASSWORD",""),
    FROM("FROM",""),
    TO("TO",""),
    SUBJECT("SUBJECT",""),
    BODY("BODY",""),


    //DataBase
    SQL_QUERY("SQL_QUERY", ""),

    JDBC_DRIVER("JDBC_DRIVER", ""),
    DB_URL("DB_URL", ""),
    DB_USER_NAME("DB_USER_NAME", ""),
    DB_PASSWORD("DB_PASSWORD", ""),


    BASE_URL_WITHDB("BASE_URL", "");

    private String key;
    private String description;

    private PropertyEnum(String key, String description){
        this.key = key;
        this.description = description;
    }

    public String getKeyText(){
        return this.key;
    }

    public String getDescription(){
        return this.description;
    }

}
