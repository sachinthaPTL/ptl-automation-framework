package com.ptl.automation.util.property;


public class AutomationProperties {
    public static String getProperty(PropertyEnum key){
        return PropertyHandle.getInstance().getValue(key.getKeyText());
    }
}
