package com.ptl.automation.util.property;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyHandle {

    private final Properties property = new Properties();


    private PropertyHandle() {
        try{
            FileInputStream in = new FileInputStream("automation.properties");
            property.load(in);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static class Handler {
        private static final PropertyHandle instance = new PropertyHandle();
    }

    public static PropertyHandle getInstance(){
        return PropertyHandle.Handler.instance;
    }

    public String getValue(String key){
        return property.getProperty(key).trim();
    }
}
