package com.ptl.automation.util.email;

import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

/**
 * Created by PTL_PC on 9/7/2017.
 */
public class ReportMail {
    public void emailReport(){

        Properties prop = new Properties();
        prop.put("mail.smtp.host","smtp.gmail.com");
        prop.put("mail.smtp.socketFactory.port","465");
        prop.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        prop.put("mail.smtp.auth","true");
        prop.put("mail.smtp.port","465");

        Session  session = Session.getDefaultInstance(prop,new javax.mail.Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication(){
                String UserName = AutomationProperties.getProperty(PropertyEnum.USERNAME);
                String Password = AutomationProperties.getProperty(PropertyEnum.PASSWORD);
                return  new PasswordAuthentication(UserName,Password);
            }
        });
        try{

            Message message = new MimeMessage(session);
            String stFrom = AutomationProperties.getProperty(PropertyEnum.FROM);
            message.setFrom(new InternetAddress(stFrom));
            String stTo = AutomationProperties.getProperty(PropertyEnum.TO);
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(stTo));
            String Subject = AutomationProperties.getProperty(PropertyEnum.SUBJECT);
            message.setSubject(Subject);

            BodyPart messageBodyPart = new MimeBodyPart();
            String Body = AutomationProperties.getProperty(PropertyEnum.BODY);
            messageBodyPart.setText(Body);
            MimeBodyPart messageMimeBodyPart = new MimeBodyPart();

            //String fileName = AutomationProperties.getProperty(PropertyEnum.EXTENDREPORT_PATH);
            String fileName="C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\Reports\\TestReport.html";
            DataSource source = new FileDataSource(fileName);
            messageMimeBodyPart.setDataHandler(new DataHandler(source));
            messageMimeBodyPart.setFileName(fileName);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageMimeBodyPart);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport.send(message);

            System.out.println("======Email Sent======");

        }

        catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }


    }

}


