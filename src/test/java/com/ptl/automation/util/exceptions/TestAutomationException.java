package com.ptl.automation.util.exceptions;

/**
 * Created by PTL on 6/14/2017.
 */
public class TestAutomationException extends RuntimeException  {

    public TestAutomationException(final String message) {
        super(message);
        //logger.error(message);
    }

    public TestAutomationException(final String message, final Throwable cause) {
        super(message, cause);
        //logger.error(message, cause);
    }
}
