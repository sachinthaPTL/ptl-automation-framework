package com.ptl.automation.util.data;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Kanchuka Tharindu  on 6/7/2017.
 */
public class CSVUtil {

    private CSVParser parser;

    public CSVUtil(String fileName) throws IOException {
        FileReader csvData = new FileReader (System.getProperty("user.dir") +"\\testdata\\" + fileName + ".csv");
        parser = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(csvData);
    }

    public List<String> getHeads() throws IOException{
        List<String> heads = new ArrayList<String>();

        Map<String, Integer> headersMap = parser.getHeaderMap();

        headersMap.forEach((k,v)->heads.add(k));

        return heads;
    }

    public Object[][] getTestData() throws IOException{
        List<CSVRecord> records = parser.getRecords();
        int columnSize = parser.getHeaderMap().size();

        Object[][] arrays = new Object[records.size()][columnSize];
        int i = 0;
        for(CSVRecord row: records){
            for(int j = 0; j < columnSize; j++){
                String cell = (row.get(j) == null)?"":row.get(j);
                arrays[i][j] = cell;
            }
            i++;
        }

        return arrays;
    }

    public String getCell(int rowIndex, String column){
        return null;
    }

    public String getCell(int rowIndex, int columnIndex){
        return null;
    }

}
