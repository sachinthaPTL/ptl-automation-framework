package com.ptl.automation.util.data;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;


/**
 * Created by RasithaE on 8/7/2016.
 */
public class ExcelHandler {

    private static Sheet workSheet;
    private static Workbook workBook;
    private static Hashtable DataHashTable=new Hashtable();
    private static File ExcelFile;
    private static String[][] Data;

   //-------------------------------------Using POI---------------------------------------------

    public List readDataTableWithHeader(String excelPath, String dataSheet, int numberOfColumns){
        List list;
        return list  = getData(excelPath,dataSheet, numberOfColumns, true);
    }

    public List readDataTableWithOutHeader(String excelPath, String dataSheet, int numberOfColumns){
        List list;
        return list  = getData(excelPath,dataSheet, numberOfColumns, false);
    }

    public List getData(String excelPath, String dataSheet, int numberOfColumns, boolean isHeadersExist) {
        List dataList = new ArrayList();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(excelPath));
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheet(dataSheet);
            Iterator rows = sheet.rowIterator();

            while (rows.hasNext()) {
                XSSFRow row = ((XSSFRow) rows.next());
                Iterator cells = row.cellIterator();
                int i = 0;
                String[] testData= new String[numberOfColumns];
                while (cells.hasNext()) {
                    XSSFCell cell = (XSSFCell) cells.next();
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    String value = cell.getStringCellValue();
                    if (!value.equals(null)) {
                        testData [i] = value;
                        i++;
                    }
                }
                dataList.add(testData);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }


    //------------------------ Using JXL---------------------------------

    public static String[][] getData(String FilePath,String SheetName,String TableName)
    {
        ExcelFile= new File(FilePath);
        try {
            workBook = Workbook.getWorkbook(ExcelFile);
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        workSheet = workBook.getSheet(SheetName);
        int Start_Row = workSheet.findCell(TableName).getRow()+1;
        int Start_Column = workSheet.findCell(TableName).getColumn()+1;
        jxl.Cell EndCell = workSheet.findCell(TableName, Start_Column, Start_Row,workSheet.getColumns(), workSheet.getRows(), false);
        int End_Row= EndCell.getRow()-1;
        int End_Column=EndCell.getColumn()-1;
        Data=new String[End_Row-Start_Row+1][End_Column-Start_Column+1];
        int r=0;
        int c=0;
        for(int row=Start_Row;row<=End_Row;row++ )
        {
            for(int col=Start_Column;col<=End_Column;col++)
            {
                Data[r][c]=workSheet.getCell(col,row ).getContents();
                c++;
            }
            r++; c=0;
        }
        return Data;
    }

}
