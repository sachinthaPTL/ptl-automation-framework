package com.ptl.automation.util.reports;
import com.ptl.automation.test.TestBase;
import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * Created by PTL_PC on 9/7/2017.
 */
public class ExtentReports extends TestBase {


        com.relevantcodes.extentreports.ExtentReports extentReports;
        ExtentTest test;

    public ExtentReports(String s) {

    }




    public void setExtentReports(){
           //extentReports = new com.relevantcodes.extentreports.ExtentReports(AutomationProperties.getProperty(PropertyEnum.EXTENDREPORT_PATH));
        extentReports= new com.relevantcodes.extentreports.ExtentReports(AutomationProperties.getProperty(PropertyEnum.EXTENDREPORT_PATH));
            //extentReports = new ExtentReports(AutomationProperties.getProperty(PropertyEnum.EXTENDREPORT_PATH));
        //String path = "C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\Reports\\TestReport.html";
       // return;
        }
        public void startTest(String testName){
            test=extentReports.startTest(testName);
        }
        public void logStatusINFO(String logStatus){
            test.log(LogStatus.INFO,logStatus);
        }
        public void logStatusPASS(String pass){
        test.log(LogStatus.PASS,pass);
        }
        public void logStatusFAIL(String fail){
        test.log(LogStatus.FAIL,fail);
        }
        public void addScreenShot(String status){

            String imagePass = test.addScreenCapture("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\ScreenShots\\Untitled.png");
            test.log(LogStatus.PASS, status, imagePass);

            //test.addScreenCapture("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\ScreenShots\\Untitled.png");
           // test.addScreenCapture(String.valueOf(extentReports= new com.relevantcodes.extentreports.ExtentReports(AutomationProperties.getProperty(PropertyEnum.SCREENSHOT_PATH))));
        }
        public void endReport(){
           extentReports.endTest(test);
        }
        public void flushReport(){
            extentReports.flush();
        }


}
