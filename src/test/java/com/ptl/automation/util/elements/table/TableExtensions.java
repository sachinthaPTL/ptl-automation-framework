package com.ptl.automation.util.elements.table;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by PTL on 6/14/2017.
 */
public class TableExtensions {
    /** The table component element. */
    private WebElement tableComponentElement;

    /**
     * Instantiates a new table component.
     * @param tableComponentElementVal
     *            the table component element
     */
    protected TableExtensions(final WebElement tableComponentElementVal) {
        this.tableComponentElement = tableComponentElementVal;
    }

    /**
     * Get number of rows in table
     * @return number of rows
     */
    public int GetRowCount()
    {
        return getAllRows().size();
    }

    /**
     * Get row from table by index.
     * @param rowIndex index of a row ( index starts from 0 )
     * @return {@code <tr>} node element
     */
    public WebElement getRow(final int rowIndex) {
        List<WebElement> rows = this.getAllRows();
        if (rowIndex <= rows.size()) {
            return rows.get(rowIndex);
        } else {
            return null;
        }
    }

    /**
     * Get all rows.
     * @return the all row elements as a List
     */
    public List<WebElement> getAllRows() {
        List<WebElement> rows = tableComponentElement.findElements(By.xpath("child::tr"));
        return rows;
    }

    /**
     * Get the row containing given text.
     * @param text The text to search
     * @return matching table row element
     */
    public WebElement getRowContainingText(final String text) {
        return tableComponentElement.findElement(By
                .xpath(".//tr[*[self::td|self::th]//text()[contains(.,\"" + text + "\")]]"));
    }

    /**
     * Get Row Element Containing Text In Column (Index Starts from 0)
     * @param text Text to search
     * @param columnIndex Column index to search
     * @return matching table row element
     */
    public WebElement getRowContainingTextInColumn(final String text, int columnIndex) {
        columnIndex++;
        String xpath = ".//tr[*[self::td|self::th][" + columnIndex + "][contains(text(),\"" + text + "\")]]";
        return tableComponentElement.findElement(By.xpath(xpath));
    }

    /**
     * Gets the cells from row.
     * @param rowFromTableComponent
     *            the row from table component
     * @return List of cells in a given row.
     */
    public List<WebElement> getCellsFromRow(final WebElement rowFromTableComponent) {
        return rowFromTableComponent.findElements(By.xpath("child::td|child::th"));
    }

    /**
     * Get column index. - [Changed to avoid the NoSuchElementException]
     * @param text Unique string to identify row and column
     * @return index of a column ( index starts from 0 )
     */
    public int getColumnIndex(final String text) {
        int colIndex = -1;
        WebElement rowWithProvidedText = getRowContainingText(text);
        List<WebElement> cols = getCellsFromRow(rowWithProvidedText);

        for (int i = 0; i < cols.size(); i++) {
            if (cols.get(i).getText().equals(text)) {
                colIndex = i;
                break;
            }
        }
        return colIndex;
    }

    /**
     * Get column index which contains the given text
     * @param text text to match
     * @return Return the first column index where the given text appears
     */
    public int getColumnIndexContainingText(String text)
    {
        int colIndex = -1;
        WebElement rowWithProvidedText = getRowContainingText(text);
        List<WebElement> cols = getCellsFromRow(rowWithProvidedText);

        for (int i = 0; i < cols.size(); i++)
        {
            if (cols.get(i).getText().contains(text))
            {
                colIndex = i;
                break;
            }
        }
        return colIndex;
    }

    /**
     * Get row index where the given text is displayed first
     * @param text text to match
     * @return Return the first row index where the given text appears
     */
    public int getRowIndexContainingText(String text)
    {
        int rowIndex = -1;
        List<WebElement> rows = getAllRows();
        WebElement row = getRowContainingText(text);
        for (int i = 0; i < rows.size(); i++)
        {
            if (rows.get(i).getText().contains(row.getText()))
            {
                rowIndex = i;
                break;
            }
        }
        return rowIndex;
    }

    /**
     * Get the element at given row index and column index
     * @param rowIndex Row index starting from 0
     * @param columnIndex Column index starting from 0
     * @return Returns the element at given row index and column index
     */
    public WebElement getCellFromRowIndexColumnIndex(int rowIndex, int columnIndex)
    {
        WebElement row = getRow(rowIndex);
        return row.findElement(By.cssSelector("td:nth-child(" + (columnIndex + 1) + ")"));
    }

    /**
     * Get the list of cell elements from a given row
     * @param rowIndex Row index to get the cells
     * @return Returns list of cells in a given row
     */
    public List<WebElement> getCellsFromRow(int rowIndex)
    {
        WebElement rowElement = getRow(rowIndex);
        return rowElement.findElements(By.xpath("child::td|child::th"));
    }

    /**
     * Get the cell element in a given row
     * @param rowElement row element
     * @param cellIndex cell index
     * @return matching cell element
     */
    public WebElement getCellFromRow(WebElement rowElement, int cellIndex)
    {
        List<WebElement> cells = this.getCellsFromRow(rowElement);
        if (cells.size() > cellIndex)
        {
            return cells.get(cellIndex);
        }
        return null;
    }

    /**
     * Get the list of cell elements in a given column
     * @param columnIndex column index, starts with 0
     * @return list of cells from all rows in the column
     */
    public List<WebElement> GetCellsFromColumn(int columnIndex)
    {
        List<WebElement> listOfElems = null;
        List<WebElement> rows = this.getAllRows();
        for (WebElement row : rows)
        {
            listOfElems.add(this.getCellFromRow(row, columnIndex));
        }
        return listOfElems;
    }

    /**
     * Get the cell element in a given row
     * @param rowElement row element
     * @param cellIndex cell index
     * @return matching cell element
     */
    public WebElement GetCellFromRow(WebElement rowElement, int cellIndex)
    {
        List<WebElement> cells = this.getCellsFromRow(rowElement);
        if (cells.size() > cellIndex)
        {
            return cells.get(cellIndex);
        }
        return null;
    }

    /**
     * Get the cell element with given text
     * @param text text
     * @return cell element
     */
    public WebElement GetCellFromText(String text)
    {
        int x = -1;
        int y = -1;
        x = getRowIndexContainingText(text);
        y = getColumnIndexContainingText(text);
        return getCellFromRowIndexColumnIndex(x, y);
    }
}
