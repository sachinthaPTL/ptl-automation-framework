package com.ptl.automation.util.elements.table;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by PTL on 6/14/2017.
 */
public class Table {
    private WebElement tableRootElement;

    public Table(final WebElement tableRoot) {
        this.tableRootElement = tableRoot;
    }

    /**
     * Body.
     * @return {@link TBody} object which represents table body
     */
    public TBody body() {
        return new TBody(getTBodyElement());
    }

    /**
     * Head.
     * @return {@link THead} object which represents table head
     */
    public THead head() {
        return new THead(getTHeadElement());
    }

    /**
     * Foot.
     * @return {@link TFoot} object which represents table foot
     */
    public TFoot foot() {
        return new TFoot(getTFootElement());
    }

    /**
     * Get the thead element.
     * @return the thead element
     */
    public WebElement getTHeadElement() {
        return tableRootElement.findElement(By.xpath("child::thead"));
    }

    /**
     * Get the tbody element.
     * @return the tbody element
     */
    public WebElement getTBodyElement() {
        return tableRootElement.findElement(By.xpath("child::tbody"));
    }

    /**
     * Get the tfoot element.
     * @return the tfoot element
     */
    public WebElement getTFootElement() {
        return tableRootElement.findElement(By.xpath("child::tfoot"));
    }
}
