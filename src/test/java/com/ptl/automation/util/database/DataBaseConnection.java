package com.ptl.automation.util.database;

import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;

import java.sql.*;

public class DataBaseConnection {

    public void databaseConnection() {
        // Object of Connection from the Database
        Connection conn = null;

        // Object of Statement. It is used to create a Statement to execute the query
        Statement stmt = null;

        //Object of ResultSet => 'It maintains a cursor that points to the current row in the result set'
        ResultSet resultSet = null;
        try{
            //STEP 1: Register JDBC driver
            Class.forName(AutomationProperties.getProperty(PropertyEnum.JDBC_DRIVER));

            //STEP 2: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(AutomationProperties.getProperty(PropertyEnum.DB_URL),
                    AutomationProperties.getProperty(PropertyEnum.DB_USER_NAME),
                    AutomationProperties.getProperty(PropertyEnum.DB_PASSWORD));

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String Sql;

            Sql = AutomationProperties.getProperty(PropertyEnum.SQL_QUERY);
            resultSet = stmt.executeQuery(Sql);

            //STEP 5: Extract data from result set
            while(resultSet.next()){
               //Your @TEST Code Here
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
     }
}

