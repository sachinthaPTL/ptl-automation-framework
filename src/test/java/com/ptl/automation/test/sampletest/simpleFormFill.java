package com.ptl.automation.test.sampletest;

import com.ptl.automation.pages.SamplePage;
import com.ptl.automation.test.TestBase;
import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import org.testng.annotations.Test;

/**
 * Created by Kanchuka Tharindu on 6/7/2017.
 */
public class simpleFormFill extends TestBase {
    private SamplePage samplePage;

    @Test
    public void sampleTest(){
        driver.get(AutomationProperties.getProperty(PropertyEnum.BASE_URL));
        samplePage = new SamplePage(driver);
        samplePage.fillDetails();
    }
}
