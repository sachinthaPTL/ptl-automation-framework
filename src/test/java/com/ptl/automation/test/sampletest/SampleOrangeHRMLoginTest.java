package com.ptl.automation.test.sampletest;
import com.ptl.automation.pages.SampleOrangeHRMLoginPage;
import com.ptl.automation.test.TestBase;
import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;


public class SampleOrangeHRMLoginTest extends TestBase {

    @Test
    public void SampleOrangeHRMLoginTest(){

        getStartTime();
        driver.get(AutomationProperties.getProperty(PropertyEnum.BASE_URL));
        SampleOrangeHRMLoginPage sampleOrangeHRMLoginPage = PageFactory.initElements(driver,SampleOrangeHRMLoginPage.class);
        sampleOrangeHRMLoginPage.typeUserName();
        sampleOrangeHRMLoginPage.typePassword();
        sampleOrangeHRMLoginPage.clickSubmit();
        sampleOrangeHRMLoginPage.assertionPass();
        getEndTime();
        totalTime();
    }

}
