package com.ptl.automation.test.sampletest;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;


/**
 * Created by Kanchuka Tharindu on 6/9/2017.
 */
public class LogTest {
    private static final Logger logger = Logger.getLogger(simpleFormFill.class);

    @Test
    public void testLoggin(){
        logger.info("info");
        logger.warn("warn");
        logger.error("error");
        logger.fatal("fatal");
    }
}
