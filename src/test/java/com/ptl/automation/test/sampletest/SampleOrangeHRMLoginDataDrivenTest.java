package com.ptl.automation.test.sampletest;
import com.ptl.automation.pages.SampleDataDrivenOrangeHRMLoginPage;
import com.ptl.automation.test.TestBase;
import com.ptl.automation.util.data.ReadExcelConfig;
import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SampleOrangeHRMLoginDataDrivenTest extends TestBase {
    private SampleDataDrivenOrangeHRMLoginPage sampleDataDrivenOrangeHRMLoginPage;


    @DataProvider(name = "FromExelSheet")
    public Object[][] passdata(){

        ReadExcelConfig reo = new ReadExcelConfig("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\ExcelTestData.xlsx");
        int rows = reo.getRowCount(0);
        Object[][] data = new Object[rows][2];
        for (int i=0;i<rows;i++){
            data[i][0] = reo.getData(0, i, 0);
            data[i][1] = reo.getData(0, i, 1);
        }
       return data;
    }

    @Test(dataProvider = "FromExelSheet")
    public void FromExel(String dataOne, String dataTwo) {
        driver.get(AutomationProperties.getProperty(PropertyEnum.BASE_URL));
        sampleDataDrivenOrangeHRMLoginPage = new SampleDataDrivenOrangeHRMLoginPage(driver);
        sampleDataDrivenOrangeHRMLoginPage.typeUserName(dataOne);
        sampleDataDrivenOrangeHRMLoginPage.typePassword(dataTwo);
        sampleDataDrivenOrangeHRMLoginPage.clickSubmit();

    }
}
