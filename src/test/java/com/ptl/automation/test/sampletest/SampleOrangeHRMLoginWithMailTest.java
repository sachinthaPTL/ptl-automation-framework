package com.ptl.automation.test.sampletest;


import com.ptl.automation.util.email.*;
import com.ptl.automation.pages.SampleOrangeHRMLoginPage;
import com.ptl.automation.test.TestBase;
import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import com.ptl.automation.util.reports.ExtentReports;
import org.testng.annotations.Test;

public class SampleOrangeHRMLoginWithMailTest extends TestBase {

    private SampleOrangeHRMLoginPage sampleOrangeHRMLoginPage;

    @Test
    public void sampleOrangeHRMLoginTest(){

        driver.get(AutomationProperties.getProperty(PropertyEnum.BASE_URL));
        ExtentReports reports = new ExtentReports(toString());
        reports.setExtentReports();
        reports.startTest("Login With Valid Credentials");
        reports.logStatusINFO("Navigate To the OrangeHRM.com");
        sampleOrangeHRMLoginPage = new SampleOrangeHRMLoginPage();
        sampleOrangeHRMLoginPage.typeUserName();
        reports.logStatusINFO("Type UserName");
        sampleOrangeHRMLoginPage.typePassword();
        reports.logStatusINFO("Type Password");
        sampleOrangeHRMLoginPage.clickSubmit();
        reports.logStatusINFO("Click Login Button");
        //String pass = sampleOrangeHRMLoginPage.assertionPass();
        if(sampleOrangeHRMLoginPage.assertionPass()==true){reports.logStatusPASS("Test Pass");
            reports.addScreenShot("Home Page");
        }else {
            reports.logStatusFAIL("Test Fail");
        }
        reports.endReport();
        reports.flushReport();
        ReportMail mail = new ReportMail();
        mail.emailReport();

    }

}
