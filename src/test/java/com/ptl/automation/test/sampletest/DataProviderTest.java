package com.ptl.automation.test.sampletest;

import com.ptl.automation.util.data.CSVUtil;
import com.ptl.automation.util.data.ExcelHandler;
import com.ptl.automation.util.data.ExcelUtils;
import com.ptl.automation.util.data.ReadExcelConfig;
import org.apache.xpath.operations.String;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Kanchuka Tharindu on 6/9/2017.
 */
public class DataProviderTest {


    /*
    * Method is used to retieve test data csv file
    * @throws IOException
    */
    @DataProvider(name = "getCSVTestData")
    public Object[][] dataProvider() throws IOException {
        return new CSVUtil("csvTestData").getTestData();
    }

    @Test(dataProvider = "getCSVTestData", enabled = true)
    public void printCSVTestData(String csvParam0, String csvParam1){
        System.out.println(csvParam0);
        System.out.println(csvParam1);
    }

//    @DataProvider(name = "getExcelTestData")
//    public Object[][] dataProvider1() throws Exception {
//        return ExcelUtils.getTableArray("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\ExcelTestData.xlsx","Sheet1");
//    }
//
//    @Test(dataProvider = "getExcelTestData", enabled = true)
//    public void printExcelTestData(String csvParam2, String csvParam3){
//        System.out.println("fhgf"+csvParam2);
//        System.out.println("fhgf"+csvParam3);
//    }






  // Working DataDriven
    @DataProvider(name = "getExcelTestData")
    public Object[][] dataProvider1() throws Exception {
        return ExcelUtils.getTableArray("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\ExcelTestData.xlsx","Sheet1");
    }


    @DataProvider(name = "FromExelSheet")
    public Object[][] passdata(){
        ReadExcelConfig reo = new ReadExcelConfig("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\ExcelTestData.xlsx");
        int rows = reo.getRowCount(0);
        Object[][] data = new Object[rows][2];
        for (int i=0;i<rows;i++){
            data[i][0] = reo.getData(0, i, 0);
            data[i][1] = reo.getData(0, i, 1);
        }
        return data;
    }

    @Test(dataProvider = "FromExelSheet")
    public void FromExel(String dataOne,String dataTwo) {
        System.out.println(dataOne);
        System.out.println(dataTwo);
    }

    @DataProvider(name="ExcelTest")
    public Object[][] createData()
    {
        return (ExcelHandler.getData("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\testdata.xls","TestDataSheet1","Sheet1Table1"));
    }

    @Test(dataProvider = "ExcelTest", enabled = true)
    public void printExcelTestDataJxl(String Param1, String Param2){
        System.out.println(Param1);
        System.out.println(Param2);
    }

    @DataProvider(name="ExcelTest1")
    public Object[][] createData1()
    {
        return (ExcelHandler.getData("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\testdata.xls","TestDataSheet1","Sheet1Table2"));
    }

    @Test(dataProvider = "ExcelTest1", enabled = true)
    public void printExcelTestDataJxl_1(String Param1, String Param2, String Param3){
        System.out.println(Param1);
        System.out.println(Param2);
        System.out.println(Param3);
    }

    @DataProvider(name="ExcelTest2")
    public Object[][] createData2()
    {
        return (ExcelHandler.getData("C:\\Users\\PTL_PC\\IdeaProjects\\PTLAutomationFramework\\TestData\\testdata.xls","TestDataSheet2","Sheet2Table1"));
    }

    @Test(dataProvider = "ExcelTest2", enabled = true)
    public void printExcelTestDataJxl_2(String Param1, String Param2){
        System.out.println(Param1);
        System.out.println(Param2);
    }

}
