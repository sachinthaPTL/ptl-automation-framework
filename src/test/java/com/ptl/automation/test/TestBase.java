package com.ptl.automation.test;

import com.ptl.automation.util.property.AutomationProperties;
import com.ptl.automation.util.property.PropertyEnum;
import org.apache.commons.lang.time.StopWatch;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


public class TestBase {

    public static WebDriver driver;

    @BeforeTest
    public void init() {
        driver = getDriver();
        driver.manage().window().maximize();

    }

    private WebDriver getDriver() {
       // WebDriver driver = null;
        /*System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\executables\\IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        return driver;*/

//        Thread t1 = new Thread();



        boolean browserFlagIE = Boolean.parseBoolean(AutomationProperties.getProperty(PropertyEnum.BROWSER_IE));
        boolean browserFlagChm = Boolean.parseBoolean(AutomationProperties.getProperty(PropertyEnum.BROWSER_CHROME));
        boolean browserFlagFireFox = Boolean.parseBoolean(AutomationProperties.getProperty(PropertyEnum.BROWSER_FIREFOX));
        boolean browserHTMLUnit = Boolean.parseBoolean(AutomationProperties.getProperty(PropertyEnum.BROWSER_HTMLUNIT));
        boolean browserFlagPhanthomJS = Boolean.parseBoolean(AutomationProperties.getProperty(PropertyEnum.BROWSER_PHANTHOMJS));

        if (browserFlagIE) {
            System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\drivers\\IEDriverServer.exe");
            driver = new InternetExplorerDriver();
        } else if (browserFlagChm ) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browserFlagFireFox) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
            driver = new FirefoxDriver();
        } else if (browserHTMLUnit) {
            driver = new HtmlUnitDriver();
        } else if (browserFlagPhanthomJS) {
            File file = new File("drivers/phantomjs.exe");
            System.setProperty("phantomjs.binary.path", file.getAbsolutePath());
            driver = new PhantomJSDriver();
        }

        return driver;
    }

    @AfterTest
    public void tearDown() {
        driver.manage().deleteAllCookies();
        driver.quit();
    }

    /*@AfterClass
    public void tearDown(){
    	driver.close();
    }*/

// Methods
//Simple Methods

    public void sendKeys(WebElement webElement, String keys) {
        webElement.sendKeys(keys);
    }

    public void click(WebElement webElement) {
        webElement.click();
    }

    public void clearText(WebElement webElement) {
        webElement.clear();
    }

    public void type(WebElement webElement, String input) {
        webElement.sendKeys(input);
    }

    public void clearAndType(WebElement webElement, String input) {
        clearText(webElement);
        type(webElement, input);
    }

    public void submit(WebElement webElement) {
        webElement.submit();
    }

    public void check(WebElement checkBox){
        if (!checkBox.isSelected()) {
            checkBox.click();
        }
    }

    public void unCheck(WebElement checkBox) {
        if (checkBox.isSelected()) {
            checkBox.click();
        }
    }

    public void sleep() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isDisplayed(WebElement webElement) {
        webElement.isDisplayed();
        return true;
    }

    public boolean isSelected(WebElement webElement) {
        webElement.isSelected();
        return true;
    }

    public boolean isEnable(WebElement webElement) {
        webElement.isEnabled();
        return true;
    }

    //Explicit Waits

    public void clickAndWait(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public boolean elementSelectionStateToBe(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.elementSelectionStateToBe(webElement, true));
        return true;
    }

    public void waitForElemntEnable(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.presenceOfElementLocated((By) webElement));
    }

    public void frameToBeAvaliableAndSwitchToIt(WebElement frame) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));

    }

    //Implicit Wait
    public void implicitWait() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void waitForAlertPresent() {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.alertIsPresent());

    }


    //Alert Handle
    public void switchOnToAlert() {
        Alert alert = driver.switchTo().alert();
    }

    public void acceptAlert() {
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public void rejectAlert() {
        Alert alert = driver.switchTo().alert();
        alert.dismiss();
    }

    public void getAlertText() {
        Alert alert = driver.switchTo().alert();
        alert.getText();
    }

    public void sendText(String text) {
        Alert alert = driver.switchTo().alert();
        alert.sendKeys(text);
    }

    //DropDown Handle
    public void dropdownHandelByValue(WebElement webElement, String value) {
        Select dropdown = new Select(webElement);
        dropdown.selectByValue(value);
    }

    public void dropdownHandelByVisibleText(WebElement webElement, String visibleText) {
        Select dropdown = new Select(webElement);
        dropdown.selectByVisibleText(visibleText);
    }

    public void deSelectByValue(WebElement webElement, String value) {
        Select dropdown = new Select(webElement);
        dropdown.deselectByValue(value);
    }

    public void deSelectByVisiblText(WebElement webElement, String visibleText) {
        Select dropdown = new Select(webElement);
        dropdown.deselectByVisibleText(visibleText);
    }

//File Uploading Using AutoIT

    public void fileUploadUsingAutoIt(WebElement webElement, String pathToExeFile) {
        webElement.click();
        try {
            Runtime.getRuntime().exec(pathToExeFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//File Uploading Using SendKeys

    public void fileUpload(WebElement fileElemnt, String filepath) {
        fileElemnt.sendKeys(filepath);
    }


    //Basic Sikiuli Handles
    public void clickImage(String PathtoImage) throws FindFailed {
        Screen screen = new Screen();
        screen.click(PathtoImage);
    }

    //Action Class
    public void moveTOElementandClick(WebElement element) {
        Actions actions = new Actions(driver);
        Action seriesOfActions = actions.moveToElement(element)
                .click()
                .build();
        seriesOfActions.perform();
    }

    public void moveToElementAndSendkeys(WebElement element, String text) {
        Actions actions = new Actions(driver);
        Action seriesOfActions = actions.moveToElement(element)
                .click()
                .sendKeys(text)
                .build();
        seriesOfActions.perform();
    }

    //Slider
    public void sliderToRight(WebElement element) {
        WebElement slider = driver.findElement((By) element);
        for (int i = 0; i <= 10; i++) {
            slider.sendKeys(Keys.ARROW_RIGHT);
        }
    }

    public void sliderToLeft(WebElement element) {
        WebElement slider = driver.findElement((By) element);
        for (int i = 0; i <= 10; i++) {
            slider.sendKeys(Keys.ARROW_LEFT);
        }
    }

    //DragandDrop
    public void dragandDrop(WebElement dragItem, WebElement dropItem) {
        Actions builder = new Actions(driver);

        Action dragAndDrop = builder.clickAndHold(dragItem)
                .moveToElement(dropItem)
                .release(dragItem)
                .build();
        dragAndDrop.perform();
    }

    //TestExecutionTime
    public long getStartTime(){
        long startTime = System.currentTimeMillis();
        return startTime;

    }
    public long getEndTime(){
        long endTime = System.currentTimeMillis();
        return endTime;
    }
    public void totalTime(){
        long totalTime = ((int) getStartTime());
        int seconds = (int) ((totalTime / 1000) % 60);
        System.out.println(seconds+ "Seconds");
    }

    //TODO
    //Reading properties from apache commons
    //Include mail configuration in property file [Ok]
    //Report file name should be a property [Ok]
    //Driver instance management when test are run parallel [Ok]
    //Include stopwatch [Ok]
    //Packages name [Ok]
    //tests and pages [Ok]
    //Directory Name [Ok]
    //Pom file mange

}
